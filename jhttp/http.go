/****************************************************************************
MIT License

Copyright (c) 2022 yjhi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/

package jhttp

import (
	"bufio"
	"net/http"
	"time"
)

// /Add By yjh 2021-06-17
type Http struct {
	Client    *http.Client
	Error     string
	Request   *http.Request
	httpError error
	isOk      bool
}

func New(url string, timeoutSecond int, way string) *Http {
	return Create(url, timeoutSecond, way)
}

func Create(url string, timeoutSecond int, way string) *Http {
	return _buildHttp(url, timeoutSecond, way, "")
}

func NewWithData(url string, timeoutSecond int, way string, data string) *Http {
	return _buildHttp(url, timeoutSecond, way, data)
}

func CreateWithData(url string, timeoutSecond int, way string, data string) *Http {
	return _buildHttp(url, timeoutSecond, way, data)
}

func CreateGET(url string, timeoutSecond int) *Http {

	return Create(url, timeoutSecond, "GET")
}

func CreatePOST(url string, timeoutSecond int) *Http {

	return Create(url, timeoutSecond, "POST")
}

func CreatePUT(url string, timeoutSecond int) *Http {

	return Create(url, timeoutSecond, "PUT")
}

func CreatePOSTWithData(url string, timeoutSecond int, data string) *Http {

	return CreateWithData(url, timeoutSecond, "POST", data)
}

func (h *Http) HttpError() (bool, error) {
	return h.isOk, h.httpError
}

func (h *Http) AddCookie(name string, value string, path string, domain string) *Http {

	if h.isOk {
		h.Request.AddCookie(&http.Cookie{
			Name:   name,
			Value:  value,
			Path:   path,
			Domain: domain,
		})
	}

	return h
}

func (h *Http) WithJsonContentType() *Http {
	return h.WithContentType("application/json")
}

func (h *Http) WithContentType(v string) *Http {
	if h.isOk {
		h.Request.Header.Set("Content-Type", v)
	}
	return h
}

func (h *Http) WithAccept(v string) *Http {
	if h.isOk {
		h.Request.Header.Set("Accept", v)
	}

	return h
}

func (h *Http) WithCharset(v string) *Http {
	if h.isOk {
		h.Request.Header.Set("Accept-Charset", v)
	}

	return h
}

func (h *Http) WithUTF8Charset() *Http {
	if h.isOk {
		h.Request.Header.Set("Accept-Charset", "utf-8")
	}

	return h
}

func (h *Http) WithKeepAlive() *Http {
	if h.isOk {
		h.Request.Header.Set("Connection", "keep-alive")
	}

	return h
}

func (h *Http) WithEventStream() *Http {
	if h.isOk {
		h.Request.Header.Set("Content-Type", "text/event-stream")
	}

	return h
}

func (h *Http) WithUserAgent(v string) *Http {
	if h.isOk {
		h.Request.Header.Set("User-Agent", v)
	}

	return h
}

func (h *Http) WithTimeout(seconds int) *Http {
	if h.isOk {
		h.Client.Timeout = time.Duration(seconds) * time.Second
	}
	return h
}

func (h *Http) WithUserAgentDefault() *Http {
	return h.WithUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36")
}

func (h *Http) AddHeader(name string, value string) *Http {
	if h.isOk {
		h.Request.Header.Set(name, value)
	}

	return h
}

func (h *Http) SetHeader(name string, value string) *Http {
	return h.AddHeader(name, value)
}

func (h *Http) WithHeader(name string, value string) *Http {
	return h.AddHeader(name, value)
}

func (h *Http) Send() (string, int, error) {
	return h._doWithBody()
}

func (h *Http) SendRequest() (string, error) {
	return h._do()
}
func (h *Http) SendRequestWithoutBody() (int, error) {
	return h._doWithoutBody()
}
func (h *Http) SendRequestWithResp() (*HttpResp, error) {
	return h._doWithResp()
}
func (h *Http) SendRequestWithBody() (string, int, error) {
	return h._doWithBody()
}

func (h *Http) SendSSE(fun func(data []byte) error) (int, error) {

	if !h.isOk {
		return 0, h.httpError
	}

	h.WithEventStream().WithUTF8Charset().WithKeepAlive()

	rsp, err := h._doWithResp()
	if err != nil {
		return 0, err
	}

	if rsp.Response.StatusCode == 200 {
		reader := bufio.NewReader(rsp.Response.Body)

		for {
			line, err := reader.ReadBytes('\n')
			if err != nil {
				return 0, err
			}

			if err := fun(line); err != nil {
				return 0, err
			}
		}
	}

	return rsp.Response.StatusCode, nil
}

type HttpConfig struct {
	Way     string
	Url     string
	Timeout int
	Data    string
	Headers map[string]string
}

func CreateWithConfig(cfg HttpConfig) *Http {

	h := _buildHttp(cfg.Url, cfg.Timeout, cfg.Way, cfg.Data)

	if h.httpError != nil {
		h.Error = h.httpError.Error()
		return h
	}

	for k, v := range cfg.Headers {
		h.SetHeader(k, v)
	}
	return h
}
