package jservice

import (
	"os/exec"
	"path"

	"golang.org/x/text/encoding/simplifiedchinese"
)

type BaseService struct {
	ExeFile string
	ExeName string
	ExePath string
	Params  string
	After   []string
}

func (b BaseService) RunCmd(name string, params ...string) (string, error) {

	cmd := exec.Command(name, params...)

	out, err := cmd.CombinedOutput()

	return string(out), err
}

func (b BaseService) RunCmdGbk(name string, params ...string) (string, error) {

	cmd := exec.Command(name, params...)

	out, err := cmd.CombinedOutput()

	bhfUtf, _ := simplifiedchinese.GB18030.NewDecoder().Bytes(out)

	return string(bhfUtf), err
}

func (b BaseService) ParseFile(file string) (string, string) {
	Directory := path.Dir(file)
	fileName := path.Base(file)
	return Directory, fileName
}
