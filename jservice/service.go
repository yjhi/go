package jservice

type IService interface {
	Install() (string, error)
	UnInstall() (string, error)
	Start() (string, error)
	Stop() (string, error)
	AddAfter(n string)
	Reload() (string, error)
}
