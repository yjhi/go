/****************************************************************************
MIT License

Copyright (c) 2022 yjhi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/

package jutils

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"runtime"
	"strings"

	"golang.org/x/text/encoding/simplifiedchinese"
)

func _runForLinuxWithErr(dir string, cmd string) (string, error) {
	c := exec.Command("/bin/sh", "*c", cmd)

	if len(dir) > 0 {
		c.Dir = dir
	}

	c.Env = os.Environ()

	c.Stderr = os.Stderr
	result, err := c.Output()
	return strings.TrimSpace(string(result)), err
}

func _runForWindowsWithErr(dir string, cmd string) (string, error) {
	c := exec.Command("cmd", "/c", cmd)
	if len(dir) > 0 {
		c.Dir = dir
	}

	c.Stderr = os.Stderr
	c.Env = os.Environ()

	result, err := c.Output()
	return strings.TrimSpace(string(result)), err
}

// run command and get result
func RunCommandWithErr(dir string, cmd string) (string, error) {
	if runtime.GOOS == "windows" {
		return _runForWindowsWithErr(dir, cmd)
	} else {
		return _runForLinuxWithErr(dir, cmd)
	}
}

type CmdUtils struct {
	Dir    string
	Cmd    string
	Params []string
}

func (c *CmdUtils) RunResult() (string, error) {

	cmd := c.Cmd

	if len(c.Params) > 0 {
		cmd = cmd + " " + strings.Join(c.Params, " ")
	}

	if runtime.GOOS == "windows" {
		return _runForWindowsWithErr(c.Dir, cmd)
	} else {
		return _runForLinuxWithErr(c.Dir, cmd)
	}
}

func (c *CmdUtils) Run() error {
	return RunCommandWithParam(c.Dir, c.Cmd, c.Params...)
}

func RunEx(c string, p ...string) error {
	cmd := exec.Command(c, p...)

	cmd.Env = os.Environ()

	if err := cmd.Start(); err != nil {
		return err
	}
	return cmd.Wait()
}

func Run(c string, p string) error {
	cmd := exec.Command(c, p)
	if err := cmd.Start(); err != nil {
		return err
	}
	return nil
}

func RunCmd(c string, p string) (error, string) {
	s, e := _runForWindowsWithErr(c, p)
	return e, s
}
func RunLinuxCmd(c string, p string) (error, string) {
	s, e := _runForLinuxWithErr(c, p)
	return e, s
}

func RunCommand(path string, args string) error {

	s := strings.Split(args, " ")

	l := len(s)

	if l > 1 {
		return RunCommandWithParam(path, s[0], s[1:]...)
	} else {
		return RunCommandWithParam(path, args)
	}

}

func RunCommandResult(path string, args string) string {
	s := strings.Split(args, " ")

	l := len(s)

	if l > 1 {
		return RunCommandWithParamAndResult(path, s[0], s[1:]...)
	} else {
		return RunCommandWithParamAndResult(path, args)
	}
}

func RunCommandWithParamAndResult(path string, cmd string, args ...string) string {
	c := exec.Command(cmd, args...)
	if len(path) > 0 {
		c.Dir = path
	}
	c.Env = os.Environ()

	stdout, stdErr := c.CombinedOutput()

	if stdErr != nil {
		fmt.Println("Error: ", stdErr)
	}

	bhfUtf, _ := simplifiedchinese.GB18030.NewDecoder().Bytes(stdout)

	return string(bhfUtf)
}

func RunCommandWithParam(path string, cmd string, args ...string) error {

	c := exec.Command(cmd, args...)

	if len(path) > 0 {
		c.Dir = path
	}

	c.Env = os.Environ()

	stdout, stdErr := c.StdoutPipe()
	c.Stderr = c.Stdout

	if stdErr != nil {
		fmt.Println("Error: ", stdErr)
		return stdErr
	}

	buf := bufio.NewScanner(stdout)
	for buf.Scan() {
		bhfUtf, _ := simplifiedchinese.GB18030.NewDecoder().Bytes(buf.Bytes())
		fmt.Printf("%s\n", bhfUtf)
	}

	if stdErr = c.Wait(); stdErr != nil {
		fmt.Println("Error: ", stdErr)
		return stdErr
	}

	return nil
}
