/****************************************************************************
MIT License

Copyright (c) 2022 yjhi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/

package jutils

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/md5"
	"crypto/sha1"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"io"
	"os"
	"strings"
)

func Md5(content string) string {
	w := md5.New()
	io.WriteString(w, content)
	md5str := fmt.Sprintf("%x", w.Sum(nil))
	return md5str
}

func CalcFileMd5(path string) (string, error) {

	file, err := os.Open(path)
	if err != nil {
		return "", nil
	}
	defer file.Close()

	md5Utils := md5.New()
	io.Copy(md5Utils, file)

	return hex.EncodeToString(md5Utils.Sum(nil)), nil

}

func CalcFileMD5(path string) (string, error) {

	s, e := CalcFileMd5(path)

	if e != nil {
		return s, e
	}

	return strings.ToUpper(s), e

}

func Sha1(data string) string {
	t := sha1.New()
	io.WriteString(t, data)
	return fmt.Sprintf("%x", t.Sum(nil))
}

func SHA1(data string) string {
	t := sha1.New()
	io.WriteString(t, data)
	return fmt.Sprintf("%X", t.Sum(nil))
}

func MD5(content string) string {
	w := md5.New()
	io.WriteString(w, content)
	md5str := fmt.Sprintf("%X", w.Sum(nil))
	return md5str
}

func _fPKCS7Padding(ciphertext []byte, blockSize int) []byte {
	padding := blockSize - len(ciphertext)%blockSize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(ciphertext, padtext...)
}

func _fPKCS7UnPadding(origData []byte) []byte {
	length := len(origData)
	unpadding := int(origData[length-1])
	return origData[:(length - unpadding)]
}

// AES加密,CBC
func AesBase64Encode(origstr string, keystr string) (string, error) {

	key := []byte(keystr)

	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	origData := []byte(origstr)
	blockSize := block.BlockSize()

	origData = _fPKCS7Padding(origData, blockSize)

	blockMode := cipher.NewCBCEncrypter(block, key[:blockSize])

	crypted := make([]byte, len(origData))
	blockMode.CryptBlocks(crypted, origData)

	return base64.StdEncoding.EncodeToString(crypted), nil
}

// AES解密
func AesBase64Decode(data string, keystr string) (string, error) {
	key := []byte(keystr)

	crypted, err := base64.StdEncoding.DecodeString(data)

	if err != nil {
		return "", err
	}

	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	blockSize := block.BlockSize()
	blockMode := cipher.NewCBCDecrypter(block, key[:blockSize])
	origData := make([]byte, len(crypted))

	blockMode.CryptBlocks(origData, crypted)
	origData = _fPKCS7UnPadding(origData)

	return string(origData), nil
}

func ToBase64(data string) string {
	return base64.StdEncoding.EncodeToString([]byte(data))
}

func FromBase64(data string) (string, error) {
	d, e := base64.StdEncoding.DecodeString(data)
	if e != nil {
		return "", e
	}
	return string(d), nil
}
