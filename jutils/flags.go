package jutils

import (
	"fmt"
	"strings"
)

// flag.Var(&names, "name", "Names,for example: -name=a -name=b")
type ArrayFlag []string

func (f *ArrayFlag) String() string {
	return fmt.Sprintf("%v", []string(*f))
}

func (f *ArrayFlag) Set(value string) error {
	*f = append(*f, value)
	return nil
}

// flag.Var(&names, "name", "Names,for example: -name a=a1 -name b=b1")
type MapFlag map[string]string

func (f MapFlag) String() string {
	return fmt.Sprintf("%v", map[string]string(f))
}

func (f MapFlag) Set(value string) error {
	split := strings.SplitN(value, "=", 2)
	f[split[0]] = split[1]
	return nil
}
