package jutils

type StringKeyUtils struct {
	_KeyMap map[string]bool
}

func BuildStringKey(keys []string) *StringKeyUtils {
	r := &StringKeyUtils{
		_KeyMap: make(map[string]bool),
	}

	for _, k := range keys {
		r._KeyMap[k] = true
	}
	return r
}

func (r *StringKeyUtils) Find(key string) bool {
	_, b := r._KeyMap[key]
	return b
}

type IntKeyUtils struct {
	_KeyMap map[int]bool
}

func BuildIntKey(keys []int) *IntKeyUtils {
	r := &IntKeyUtils{
		_KeyMap: make(map[int]bool),
	}

	for _, k := range keys {
		r._KeyMap[k] = true
	}
	return r
}

func (r *IntKeyUtils) Find(key int) bool {
	_, b := r._KeyMap[key]
	return b
}
