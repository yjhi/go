//go:build linux
// +build linux

package jutils

import (
	"io"
	"os"
	"os/exec"
)

func RunCmdWithErrEx(c string, p ...string) (error, string) {

	cmd := exec.Command(c, p...)

	stdout, err := cmd.StdoutPipe()

	if err != nil {
		return err, ""
	}
	cmd.Env = os.Environ()

	defer stdout.Close()

	if err := cmd.Start(); err != nil {

		return err, ""

	}

	if opBytes, err := io.ReadAll(stdout); err != nil {

		return err, ""

	} else {

		return nil, string(opBytes)

	}
}
