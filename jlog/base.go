package jlog

const (
	LOG_DAY int8 = 1 + iota
	LOG_MONTH
	LOG_SINGLE
)

type LogHandler func(tm string, level string, message string)

type LogInterface interface {
	Use(fun LogHandler)
	Close()
	LogFileName() string
	Debug(content string)
	Info(content string)
	Warn(content string)
	Error(content string)
	Critical(content string)
	Log(level int8, content string)
	Logs(level int8, content ...string)
	LogJson(level int8, content interface{})
	LogV(level int8, content ...interface{})
}

func (b LogConfig) SetBuffer(buffer bool, flush int) {
	b.Buffer = buffer
	b.FlushCount = flush

	if flush < 1 {
		b.FlushCount = 1
	}
}

func (b LogConfig) SetLevel(level int8) {
	b.Level = level
}

func (b LogConfig) SetHandler(handler LogHandler) {
	b.Handler = handler
}
