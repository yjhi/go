/****************************************************************************
MIT License

Copyright (c) 2022 yjhi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/

package jlog

const (
	CRITICAL int8 = 1 + iota
	ERROR
	WARN
	INFO
	DEBUG
)

var LogLevelValue map[string]int8 = map[string]int8{
	"CRITICAL": 1,
	"ERROR":    2,
	"WARN":     3,
	"INFO":     4,
	"DEBUG":    5,
}

var LogLevelName map[int8]string = map[int8]string{
	1: "CRITICAL",
	2: "ERROR",
	3: "WARN",
	4: "INFO",
	5: "DEBUG",
}

var DefLogLevelName string = "INFO"
var DefLogLevelValue int8 = INFO

func GetLevelValue(name string) int8 {
	n, e := LogLevelValue[name]
	if e {
		return n
	}

	return DefLogLevelValue
}
func GetLevelName(level int8) string {
	n, e := LogLevelName[level]
	if e {
		return n
	}

	return DefLogLevelName
}
