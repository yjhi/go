/****************************************************************************
MIT License

Copyright (c) 2022 yjhi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/

package jlog

import (
	"fmt"
	"os"
	"strings"
	"time"
)

func BuildLogWithConfig(config LogConfig) *LogUtils {
	l := _buildLogUtils(config)
	l.SetBuffer(config.Buffer, config.FlushCount)
	return l
}

// build day log
func BuildDayLogUtils(path string, name string, tail string) *LogUtils {
	return BuildLogWithConfig(LogConfig{
		LogPath:    path,
		LogName:    name,
		LogTail:    tail,
		LogType:    LOG_DAY,
		Buffer:     true,
		FlushCount: 20,
		Level:      DEBUG,
	})
}

// build month log
func BuildMonthLogUtils(path string, name string, tail string) *LogUtils {
	return BuildLogWithConfig(LogConfig{
		LogPath:    path,
		LogName:    name,
		LogTail:    tail,
		LogType:    LOG_MONTH,
		Buffer:     true,
		FlushCount: 20,
		Level:      DEBUG,
	})
}

// build single log
func BuildSingleLogUtils(path string, name string, tail string) *LogUtils {
	return BuildLogWithConfig(LogConfig{
		LogPath:    path,
		LogName:    name,
		LogTail:    tail,
		LogType:    LOG_SINGLE,
		Buffer:     true,
		FlushCount: 20,
		Level:      DEBUG,
	})
}

func Build(path string, name string, tail string, tp int8) *LogUtils {
	return BuildLogWithConfig(LogConfig{
		LogPath:    path,
		LogName:    name,
		LogTail:    tail,
		LogType:    tp,
		Buffer:     true,
		FlushCount: 20,
		Level:      DEBUG,
	})
}

func BuildMultLog(path string, name string, tail string, level int8) *MultLog {
	return _buildMultLog(LogConfig{
		LogPath:    path,
		LogName:    name,
		LogTail:    tail,
		Level:      level,
		Buffer:     true,
		FlushCount: 20,
	})
}

func BuildMultWithConfig(config LogConfig) *MultLog {
	return _buildMultLog(config)
}

func WriteAll(file string, content ...string) error {
	f, e := os.OpenFile(file, os.O_WRONLY|os.O_CREATE, 0644)
	if e != nil {
		return e
	}
	if len(content) > 1 {
		var b strings.Builder
		for _, v := range content {
			b.WriteString(v)
		}
		f.WriteString(b.String())
	} else {
		f.WriteString(content[0])
	}
	f.Close()

	return nil
}

func AppendAll(file string, content ...string) error {
	f, e := os.OpenFile(file, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if e != nil {
		return e
	}
	defer f.Close()

	if len(content) > 1 {
		var b strings.Builder
		for _, v := range content {
			b.WriteString(v)
		}
		f.WriteString(b.String())
	} else {
		f.WriteString(content[0])
	}
	return nil
}

func LogFormatV(content ...interface{}) string {
	var b strings.Builder

	for _, v := range content {
		b.WriteString(fmt.Sprintf("%+v\t", v))
	}
	return b.String()
}
func LogFormat(content ...string) string {
	var b strings.Builder
	for _, v := range content {
		b.WriteString(v + "\t")
	}
	return b.String()
}

// format log
// format {0} {1} {2}
func FormatLogs(format string, content ...string) string {
	ret := format
	for inx, s := range content {
		ret = strings.Replace(ret, fmt.Sprintf("{%d}", inx), s, -1)
	}
	return ret
}

func LogInt(s int) string {
	return fmt.Sprintf("%d", s)
}

func LogDuration(s time.Duration) string {
	return fmt.Sprintf("%d", s)
}

func LogFloat(s float64) string {
	return fmt.Sprintf("%f", s)
}

func LogValue(s interface{}) string {
	return fmt.Sprintf("%+v", s)
}
