/****************************************************************************
MIT License

Copyright (c) 2022 yjhi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/

package jlog

import (
	"bufio"
	"fmt"
	"os"

	"gitee.com/yjhi/go/jtime"
)

type logPri struct {
	_logFile    *os.File
	_fullPath   string
	_logType    int8
	_writer     *bufio.Writer
	_flushIndex int
}

var _timeUtil *jtime.TimeUtils = nil

func _getTimeUtil() *jtime.TimeUtils {
	if _timeUtil == nil {
		_timeUtil = jtime.BuildTimeUtilsNoFmt()
	}

	return _timeUtil
}

func _logDayName(tail string) string {

	logName := _getTimeUtil().Date()
	return logName + "." + tail
}

func _logMonthName(tail string) string {

	logName := _getTimeUtil().DateWithFmt("yyyyMM")
	return logName + "." + tail
}

func _writeErrorLog(content ...string) {
	fmt.Println(LogFormat(content...))
	AppendAll("error.log", content...)
}

func _buildLogUtils(cfg LogConfig) *LogUtils {

	return &LogUtils{
		LogConfig: cfg,
		LastError: nil,
		logPri: logPri{
			_fullPath:   "",
			_logType:    cfg.LogType,
			_logFile:    nil,
			_writer:     nil,
			_flushIndex: 0,
		},
	}
}

// create file function
func (l *LogUtils) _openLog(flag string) bool {

	l._logFile, l.LastError = os.OpenFile(l._fullPath, os.O_CREATE|os.O_RDWR|os.O_APPEND, 0644)

	if l.LastError != nil {

		err := "failed to create file " + flag + " " + l.LastError.Error()
		_writeErrorLog(err + "\n")
		l._logFile = nil
		return false
	}

	return true
}

// write file function
func (l *LogUtils) _writeContent(flag string, content string) {

	tm := jtime.NowDateTime()
	if l != nil && l.Handler != nil {
		l.Handler(tm, flag, content)
	}

	if l.Buffer {
		if l._writer != nil {
			if _, l.LastError = l._writer.WriteString(tm + " [" + flag + "] " + content + "\n"); l.LastError != nil {
				err := "failed to write file " + l.LastError.Error()
				_writeErrorLog(err + "\n")
			}

			l._flushForBuffer()
		}
	} else {
		if l._logFile != nil {
			if _, l.LastError = l._logFile.WriteString(tm + " [" + flag + "] " + content + "\n"); l.LastError != nil {
				err := "failed to write file " + l.LastError.Error()
				_writeErrorLog(err + "\n")
			}

			l._logFile.Sync()
		}
	}
}

// write file function
func (l *LogUtils) _writeRawContent(flag string, content string) {

	tm := jtime.NowDateTime()
	if l != nil && l.Handler != nil {
		l.Handler(tm, flag, content)
	}
	if l.Buffer {
		if l._writer != nil {
			if len(flag) > 0 {
				if _, l.LastError = l._writer.WriteString(tm + " [" + flag + "]"); l.LastError != nil {
					err := "failed to write file " + l.LastError.Error()
					_writeErrorLog(err + "\n")
				}
			} else {
				if _, l.LastError = l._writer.WriteString(" " + content); l.LastError != nil {
					err := "failed to write file " + l.LastError.Error()
					_writeErrorLog(err + "\n")
				}
			}

			l._flushForBuffer()
		}
	} else {
		if l._logFile != nil {
			if len(flag) > 0 {
				if _, l.LastError = l._logFile.WriteString(tm + " [" + flag + "]"); l.LastError != nil {
					err := "failed to write file " + l.LastError.Error()
					_writeErrorLog(err + "\n")
				}
			} else {
				if _, l.LastError = l._logFile.WriteString(" " + content); l.LastError != nil {
					err := "failed to write file " + l.LastError.Error()
					_writeErrorLog(err + "\n")
				}
			}

			l._logFile.Sync()
		}
	}
}

// build log path
func (l *LogUtils) _buildLogFullPath() string {

	var logFile string

	switch l._logType {
	case LOG_SINGLE:
		logFile = l.LogPath + "/" + l.LogName + "." + l.LogTail
	case LOG_MONTH:
		logFile = l.LogPath + "/" + l.LogName + "_" + _logMonthName(l.LogTail)
	default:
		logFile = l.LogPath + "/" + l.LogName + "_" + _logDayName(l.LogTail)
	}
	return logFile
}

func (l *LogUtils) _flushForBuffer() {
	if l != nil && l._logFile != nil {
		l._flushIndex = l._flushIndex + 1

		if l._flushIndex == l.FlushCount {
			l._flushIndex = 0
			l.Flush()
		}
	}

}

// write content
func (l *LogUtils) _writeLevelLog(level int8, content string) {
	if level <= l.Level {
		l._writeLog(GetLevelName(level), content)
	}
}

func (l *LogUtils) _checkAndOpenFile() {
	logfile := l._buildLogFullPath()
	if l._logFile == nil {
		l._fullPath = logfile
		if l._openLog("01") {
			l._writer = bufio.NewWriter(l._logFile)
		} else {
			l._writer = bufio.NewWriter(os.Stderr)
		}

	} else {
		if l._fullPath != logfile {

			if l._writer != nil {
				l._writer.Flush()
			}

			l._logFile.Sync()

			l._logFile.Close()
			l._fullPath = logfile
			if l._openLog("02") {
				l._writer.Reset(l._logFile)
			} else {
				l._writer = bufio.NewWriter(os.Stderr)
			}
		}
	}
}

// write content to file
func (l *LogUtils) _writeLog(flag string, content string) {
	l._checkAndOpenFile()
	l._writeContent(flag, content)
}

// write content to file
func (l *LogUtils) _writeRawLog(flag string, content string) {
	l._checkAndOpenFile()
	l._writeRawContent(flag, content)
}
