package jlog

type multPri struct {
	_logFile map[int8]*LogUtils
}

func (m *MultLog) _initMultLogFile() {

	if m.Buffer {
		if m.FlushCount < 1 {
			m.FlushCount = 1
		}
	}

	m._logFile = make(map[int8]*LogUtils)

	m._logFile[CRITICAL] = &LogUtils{
		LogConfig: LogConfig{
			LogName:    "crit_" + m.LogName,
			LogPath:    m.LogPath,
			LogTail:    m.LogTail,
			Level:      m.Level,
			Buffer:     m.Buffer,
			FlushCount: m.FlushCount,
			LogType:    LOG_DAY,
			Handler:    nil,
		},
	}

	m._logFile[ERROR] = &LogUtils{
		LogConfig: LogConfig{
			LogName:    "error_" + m.LogName,
			LogPath:    m.LogPath,
			LogTail:    m.LogTail,
			Level:      m.Level,
			Buffer:     m.Buffer,
			FlushCount: m.FlushCount,
			LogType:    LOG_DAY,
			Handler:    nil,
		},
	}

	m._logFile[WARN] = &LogUtils{
		LogConfig: LogConfig{
			LogName:    "warn_" + m.LogName,
			LogPath:    m.LogPath,
			LogTail:    m.LogTail,
			Level:      m.Level,
			Buffer:     m.Buffer,
			FlushCount: m.FlushCount,
			LogType:    LOG_DAY,
			Handler:    nil,
		},
	}

	m._logFile[INFO] = &LogUtils{
		LogConfig: LogConfig{
			LogName:    "info_" + m.LogName,
			LogPath:    m.LogPath,
			LogTail:    m.LogTail,
			Level:      m.Level,
			Buffer:     m.Buffer,
			FlushCount: m.FlushCount,
			LogType:    LOG_DAY,
			Handler:    nil,
		},
	}

	m._logFile[DEBUG] = &LogUtils{
		LogConfig: LogConfig{
			LogName:    "debug_" + m.LogName,
			LogPath:    m.LogPath,
			LogTail:    m.LogTail,
			Level:      m.Level,
			Buffer:     m.Buffer,
			FlushCount: m.FlushCount,
			LogType:    LOG_DAY,
			Handler:    nil,
		},
	}

}

func _buildMultLog(cfg LogConfig) *MultLog {
	m := &MultLog{
		LogConfig: cfg,
	}
	m._initMultLogFile()

	return m
}
