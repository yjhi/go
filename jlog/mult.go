/****************************************************************************
MIT License

Copyright (c) 2022 yjhi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/

package jlog

import (
	"encoding/json"
	"io"
)

type MultLog struct {
	LogConfig
	multPri
}

func (l *MultLog) WithLevel(level int8) *MultLog {
	l.Level = level

	if l._logFile != nil && len(l._logFile) > 0 {
		for _, f := range l._logFile {
			f.WithLevel(level)
		}
	}

	return l
}

func (l *MultLog) Flush() {
	if l != nil {
		if l._logFile != nil && len(l._logFile) > 0 {
			for _, f := range l._logFile {
				f.Flush()
			}
		}
	}
}

// hook
func (l *MultLog) Use(fun LogHandler) {
	l.Handler = fun
	if l._logFile != nil && len(l._logFile) > 0 {
		for _, f := range l._logFile {
			f.Use(fun)
		}
	}
}

// close file
func (l *MultLog) Close() {
	if l._logFile != nil && len(l._logFile) > 0 {
		for _, f := range l._logFile {
			if f != nil {
				f.Close()
			}
		}
	}
}

// debug
func (l *MultLog) Debug(content string) {
	if l != nil && l._logFile[DEBUG] != nil {
		l._logFile[DEBUG]._writeLevelLog(DEBUG, content)
	}
}

// info
func (l *MultLog) Info(content string) {
	if l != nil && l._logFile[INFO] != nil {
		l._logFile[INFO]._writeLevelLog(INFO, content)
	}
}

// warn
func (l *MultLog) Warn(content string) {
	if l != nil && l._logFile[WARN] != nil {
		l._logFile[WARN]._writeLevelLog(WARN, content)
	}
}

// error
func (l *MultLog) Error(content string) {
	if l != nil && l._logFile[ERROR] != nil {
		l._logFile[ERROR]._writeLevelLog(ERROR, content)
	}
}

// critical
func (l *MultLog) Critical(content string) {
	if l != nil && l._logFile[CRITICAL] != nil {
		l._logFile[CRITICAL]._writeLevelLog(CRITICAL, content)
	}
}

// log
func (l *MultLog) Log(level int8, content string) {
	if l != nil && l._logFile[level] != nil {
		l._logFile[level]._writeLevelLog(level, content)
	}
}

// logs
func (l *MultLog) Logs(level int8, content ...string) {
	if level <= l.Level {
		b := LogFormat(content...)

		if l != nil && l._logFile[level] != nil {
			l._logFile[level]._writeLog(GetLevelName(level), b)
		}
	}
}

func (l *MultLog) LogJson(level int8, content interface{}) {
	if level <= l.Level {
		retStr, retErr := json.Marshal(content)
		if retErr != nil {

			if l != nil && l._logFile[level] != nil {
				l._logFile[level]._writeLog(GetLevelName(level), retErr.Error())
			}

		} else {
			if l != nil && l._logFile[level] != nil {
				l._logFile[level]._writeLog(GetLevelName(level), string(retStr))
			}
		}
	}
}

func (l *MultLog) LogV(level int8, content ...interface{}) {
	if level <= l.Level {
		b := LogFormatV(content...)
		if l != nil && l._logFile[level] != nil {
			l._logFile[level]._writeLog(GetLevelName(level), b)
		}
	}
}

func (l *MultLog) WriteLevel(level int8) *MultLog {
	if level <= l.Level {
		l._logFile[level]._writeRawLog(GetLevelName(level), "")
	}
	return l
}

func (l *MultLog) WriteContent(level int8, content string) *MultLog {
	if level <= l.Level {
		l._logFile[level]._writeRawLog("", content)
	}
	return l
}

func (l *MultLog) Writer(level int8) io.Writer {
	return l._logFile[level]
}

// format {0} {1} {2}
func (l *MultLog) LogF(level int8, format string, content ...string) *MultLog {
	if level <= l.Level {
		l._logFile[level]._writeLog(GetLevelName(level), FormatLogs(format, content...))
	}
	return l
}
