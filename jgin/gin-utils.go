package jgin

import "github.com/gin-gonic/gin"

// 获取请求参数
func TryGetQueryString(c *gin.Context, names ...string) string {
	if len(names) == 0 {
		return ""
	}
	for _, name := range names {
		v := c.Query(name)
		if len(v) > 0 {
			return v
		}
	}
	return ""
}
