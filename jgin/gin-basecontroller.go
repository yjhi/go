package jgin

import (
	"errors"
	"strings"

	"gitee.com/yjhi/go/jtime"

	"net/http"

	"github.com/gin-gonic/gin"
)

type IBaseController interface {
	Register(server *GinServer)
}

type BaseController struct {
	IBaseController
	Name string `json:"name"`
}

type DataController struct {
	BaseController
}

func (b *DataController) ReturnError(c *gin.Context, code int, message error) {

	if message != nil {
		c.JSON(http.StatusOK, HttpResponse{
			Code:    code,
			Message: message.Error(),
		})
	} else {
		c.JSON(http.StatusOK, HttpResponse{
			Code:    code,
			Message: "error is nil",
		})
	}

}

func (b *DataController) ReturnMessage(c *gin.Context, code int, message string) {
	c.JSON(http.StatusOK, HttpResponse{
		Code:    code,
		Message: message,
	})
}

func (b *DataController) ReturnDataMessage(c *gin.Context, code int, message string, data interface{}) {
	c.JSON(http.StatusOK, HttpDataResponse{
		HttpResponse: HttpResponse{
			Code:    code,
			Message: message,
		},
		Data: data,
	})
}

func (b *DataController) ReturnPageMessage(c *gin.Context,
	code int, message string,
	pageSize int, pageNo int,
	totalCount int, totalPage int,
	data interface{}) {

	c.JSON(http.StatusOK, HttpPageResponse{
		HttpResponse: HttpResponse{
			Code:    code,
			Message: message,
		},
		PageSize:   pageSize,
		PageNo:     pageNo,
		TotalCount: totalCount,
		TotalPage:  totalPage,
		Data:       data,
	})
}

func (b *DataController) Upload(c *gin.Context, filed string, path string, name string) (string, error) {
	file, err := c.FormFile(filed)

	if err != nil {
		return "", nil
	}

	if len(name) == 0 {
		name = file.Filename
	} else {
		if strings.Contains(file.Filename, ".") {
			ary := strings.Split(file.Filename, ".")
			name = name + "." + ary[len(ary)-1]
		}
	}
	newName := ""
	if strings.HasSuffix(path, "/") {
		newName = path + name
	} else {
		newName = path + "/" + name
	}
	e := c.SaveUploadedFile(file, newName)

	if e != nil {
		return "", errors.New("upload file (" + file.Filename + ")fail,error:" + e.Error())
	}

	return name, nil
}

func (b *DataController) Uploads(c *gin.Context, filed string, path string) (map[string]string, error) {
	retFile := make(map[string]string)

	form, err := c.MultipartForm()

	if err != nil {
		return retFile, err
	}

	files := form.File[filed]

	var errStr strings.Builder
	for _, f := range files {
		newPathName := ""
		newExt := ""

		if strings.Contains(f.Filename, ".") {
			ary := strings.Split(f.Filename, ".")
			newExt = ary[len(ary)-1]
		}

		newName := jtime.BuildTimeUtilsNoFmt().DateTime() + jtime.UnixTimeStr() + "." + newExt

		if strings.HasSuffix(path, "/") {
			newPathName = path + newName
		} else {
			newPathName = path + "/" + newName
		}
		e := c.SaveUploadedFile(f, newPathName)

		if e != nil {
			errStr.WriteString("upload file(" + f.Filename + ")fail,error:" + e.Error() + "\n")
		} else {
			retFile[f.Filename] = newName
		}
	}

	errRet := errStr.String()

	if len(errRet) > 0 {
		return retFile, errors.New(errRet)
	}

	return retFile, nil
}
