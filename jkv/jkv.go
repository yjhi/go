package jkv

import (
	"encoding/json"
	"errors"

	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/util"
)

// open the database
func (j *_LevelDb) Open() error {
	if j.Instance != nil {
		return nil
	}

	j.Instance, j.Error = leveldb.OpenFile(j._dbFile, nil)

	return j.Error
}

// Close the database
func (j *_LevelDb) Close() {
	if j.Instance != nil {
		j.Instance.Close()
		j.Instance = nil
	}
}

// set key
func (j *_LevelDb) Set(key string, value string) error {
	if err := checkErr(j); err != nil {
		return err
	}
	return j.Instance.Put([]byte(key), []byte(value), nil)
}

// set keys
func (j *_LevelDb) Sets(keys map[string]string) error {
	if err := checkErr(j); err != nil {
		return err
	}

	b := leveldb.Batch{}

	for k, v := range keys {
		b.Put([]byte(k), []byte(v))
	}

	return j.Instance.Write(&b, nil)
}

// delete key
func (j *_LevelDb) Del(key string) error {
	if err := checkErr(j); err != nil {
		return err
	}

	return j.Instance.Delete([]byte(key), nil)
}

// delete keys
func (j *_LevelDb) Dels(keys []string) error {
	if err := checkErr(j); err != nil {
		return err
	}

	b := leveldb.Batch{}

	for _, k := range keys {
		b.Delete([]byte(k))
	}

	return j.Instance.Write(&b, nil)

}

// get key
func (j *_LevelDb) Get(key string) (string, error) {
	if err := checkErr(j); err != nil {
		return "", err
	}

	v, e := j.Instance.Get([]byte(key), nil)
	if e != nil {
		return "", e
	}

	return string(v), nil
}

func (j *_LevelDb) GetOrEmpty(key string) string {
	if err := checkErr(j); err != nil {
		return ""
	}

	v, e := j.Get(key)

	if e != nil {
		return ""
	}
	return v
}

func (j *_LevelDb) GetOrDefault(key string, def string) string {
	if err := checkErr(j); err != nil {
		return def
	}

	v, e := j.Get(key)

	if e != nil {
		return def
	}

	return v
}

// get keys
func (j *_LevelDb) Gets(keys []string) (map[string]string, error) {
	if err := checkErr(j); err != nil {
		return nil, err
	}

	data := make(map[string]string, 0)

	for _, k := range keys {
		v, e := j.Instance.Get([]byte(k), nil)
		if e != nil {
			data[k] = ""
		} else {
			data[k] = string(v)
		}
	}

	return data, nil
}

// set json
func (j *_LevelDb) SetJson(key string, value interface{}) error {
	if err := checkErr(j); err != nil {
		return err
	}

	jv, e := json.Marshal(value)

	if e != nil {
		return e
	}

	return j.Instance.Put([]byte(key), jv, nil)
}

// get json
func (j *_LevelDb) GetJson(key string, value interface{}) error {
	if err := checkErr(j); err != nil {
		return err
	}

	v, e := j.Instance.Get([]byte(key), nil)
	if e != nil {
		return e
	}

	if len(v) == 0 {
		return errors.New("value is empty")
	}

	jsonErr := json.Unmarshal(v, value)

	if jsonErr != nil {
		return jsonErr
	}

	return nil
}

// foreach
func (j *_LevelDb) Foreach(pre string, handler func(k []byte, v []byte)) error {
	if err := checkErr(j); err != nil {
		return err
	}

	if len(pre) > 0 {
		it := j.Instance.NewIterator(util.BytesPrefix([]byte(pre)), nil)
		for it.Next() {
			handler(it.Key(), it.Value())
		}
		it.Release()
	} else {
		it := j.Instance.NewIterator(nil, nil)

		for it.Next() {
			handler(it.Key(), it.Value())
		}

		it.Release()
	}
	return nil
}

// has
func (j *_LevelDb) Has(key string) (bool, error) {
	if err := checkErr(j); err != nil {
		return false, err
	}

	ret, retErr := j.Instance.Has([]byte(key), nil)

	if retErr != nil {
		return false, retErr
	}

	return ret, nil
}
