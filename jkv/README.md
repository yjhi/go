# jkv

#### 介绍

基于GoLevelDB的简单封装

```

type Config struct {
	Str  string                 `json:"str"`
	Db   float32                `json:"db"`
	Int  int                    `json:"int"`
	Ary  []string               `json:"ary"`
	IAry []int                  `json:"iary"`
	FAry []float32              `json:"fary"`
	Map  map[string]interface{} `json:"map"`
}


	k := jkv.Build("data1").Open()

	if read {
		d := k.GetOrEmpty("t1")

		fmt.Println("Read:", d)

		d1 := k.GetOrEmpty("t4")

		fmt.Println("Read:", d1)

		cf2 := Config{}

		k.GetJson("js", &cf2)

		bb, _ := json.Marshal(cf2)
		fmt.Println("Read Json from Config,", string(bb))

	} else {

		ipt := ""

		fmt.Scanf("%s", &ipt)

		k.Set("t1", "set-key-"+ipt)

		k.Sets(map[string]string{
			"t2": "weoiwe",
			"t3": "weew8329879",
			"t4": "weoifow37627489-" + ipt,
		})

		cf := Config{
			Str:  "yjhi",
			Db:   2.456,
			Int:  23,
			Ary:  []string{"ewe", "3432", "55555", ipt},
			IAry: []int{23, 2, 5, 78, 0, 23, 45, 789, 32342, 23446645, 657557, 12},
			FAry: []float32{0, 1, 2, 3, 4, 5, 2.45, 5.89, 3.45},
			Map: map[string]interface{}{
				"name":     "yjhi-" + ipt,
				"age":      18,
				"sex":      1,
				"gender":   "male",
				"birthday": "Sun" + ipt,
			},
		}

		k.SetJson("js", cf)
	}

```