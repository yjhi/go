package jkv

import "github.com/syndtr/goleveldb/leveldb"

type _LevelDb struct {
	ILevelDb
	Instance *leveldb.DB
	_dbFile  string
	Error    error
}
