/*
***************************************************************************
MIT License

# Copyright (c) 2022 yjhi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
****************************************************************************
*/

package jkv

import (
	"errors"
)

type ILevelDb interface {
	Open() error
	Close()
	Foreach(pre string, handler func(k []byte, v []byte)) error
	Has(key string) (bool, error)
	GetJson(key string, value interface{}) error
	SetJson(key string, value interface{}) error
	Gets(keys []string) (map[string]string, error)
	GetOrDefault(key string, def string) string
	GetOrEmpty(key string) string
	Get(key string) (string, error)
	Dels(keys []string) error
	Del(key string) error
	Sets(keys map[string]string) error
	Set(key string, value string) error
}

func New(file string) ILevelDb {
	return &_LevelDb{
		_dbFile: file,
	}
}

func Build(file string) ILevelDb {
	return &_LevelDb{
		_dbFile: file,
	}
}

// check err
func checkErr(j *_LevelDb) error {
	if j == nil {
		return errors.New("database not initialized")
	}

	if j.Instance == nil {
		return errors.New("database not opened")
	}

	return nil
}
