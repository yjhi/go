/****************************************************************************
MIT License

Copyright (c) 2022 yjhi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/
package jredis

import (
	"errors"
	"time"

	"github.com/gomodule/redigo/redis"
)

type RedisPool struct {
	Pool *redis.Pool
	RedisConfig
}

func (r RedisPool) GetConnection() redis.Conn {
	return r.Pool.Get()
}

func NewRedisPool(cfg RedisPoolConfig) *RedisPool {

	return &RedisPool{
		Pool: &redis.Pool{
			MaxIdle:     cfg.MaxIdle,
			IdleTimeout: time.Duration(cfg.IdleTimeout) * time.Second,
			MaxActive:   cfg.MaxActive,
			Dial: func() (redis.Conn, error) {
				return _CreateRedisConnect(&cfg.RedisConfig)
			},
			TestOnBorrow: func(c redis.Conn, t time.Time) error {
				_, er := c.Do("PING")
				return er
			},
			MaxConnLifetime: time.Duration(1) * time.Minute,
		},
	}
}

func NewRedisPoolDefault(cfg RedisConfig) *RedisPool {
	return NewRedisPool(RedisPoolConfig{
		MaxIdle:     20,
		IdleTimeout: 10,
		MaxActive:   0,
		RedisConfig: cfg,
	})
}

func (g *RedisPool) Check() error {
	if g == nil {
		return errors.New("Redis初始化失败！")
	}
	c := g.GetConnection()
	defer c.Close()

	_, er := redis.String(c.Do("PING"))

	if er != nil {
		return errors.New("Redis连接失败")
	}

	return nil
}
