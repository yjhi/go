package jkv

import (
	"encoding/json"
	"errors"
	"time"

	bolt "github.com/boltdb/bolt"
)

type _BoltDB struct {
	IKvUtils
	db     *bolt.DB
	DbFile string
	Bucket []byte
	isOpen bool
}

func (b *_BoltDB) Select(key string) error {
	if !b.isOpen {
		return errors.New("db is not open")
	}
	if len(key) == 0 {
		b.Bucket = []byte("default")
	} else {
		b.Bucket = []byte(key)
	}

	er := b.db.Update(func(tx *bolt.Tx) error {

		bucket := tx.Bucket(b.Bucket)
		if bucket == nil {
			_, err := tx.CreateBucket(b.Bucket)
			if err != nil {
				return err
			}
		}
		return nil
	})

	return er
}

func (b *_BoltDB) Open() error {

	var err error
	b.db, err = bolt.Open(b.DbFile, 0600, &bolt.Options{Timeout: 10 * time.Second})
	if err != nil {
		return err
	}

	b.isOpen = true

	return b.Select(string(b.Bucket))
}

func (b *_BoltDB) Close() error {
	if b.db == nil {
		return nil
	}
	return b.db.Close()
}

func (b *_BoltDB) SetBytes(key []byte, value []byte, second int64) error {
	if !b.isOpen {
		return errors.New("db is not open")
	}

	if b.db == nil {
		return errors.New("db is nil")
	}

	return b.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("default"))

		if b == nil {
			return errors.New("bucket is nil")
		}

		if second > 0 {
			err := b.Put(key, value)
			if err != nil {
				return err
			}
		} else {
			err := b.Put(key, value)
			if err != nil {
				return err
			}
		}

		return nil
	})

}

func (b *_BoltDB) Set(key string, value string, second int64) error {
	return b.SetBytes([]byte(key), []byte(value), second)
}

func (b *_BoltDB) Get(key string) (string, error) {
	if !b.isOpen {
		return "", errors.New("db is not open")
	}
	if b.db == nil {
		return "", errors.New("db is nil")
	}
	value := ""
	err := b.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("default"))
		if b != nil {
			v := b.Get([]byte(key))
			if v != nil {
				value = string(v)
				return nil
			}

			return nil

		}
		return errors.New("bucket not found")
	})

	return value, err
}

func (b *_BoltDB) GetDef(key string, def string) (string, error) {
	k, e := b.Get(key)
	if e != nil {
		return def, e
	}
	return k, nil
}

func (b *_BoltDB) Del(key string) error {
	if !b.isOpen {
		return errors.New("db is not open")
	}

	if b.db == nil {
		return errors.New("db is nil")
	}

	return b.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("default"))

		if b != nil {
			err := b.Delete([]byte(key))
			if err != nil {
				return err
			}
		}
		return nil
	})
}

func (b *_BoltDB) Has(key string) (bool, error) {

	if !b.isOpen {
		return false, errors.New("db is not open")
	}

	if b.db == nil {
		return false, errors.New("bucket is nil")
	}
	value := false
	err := b.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("default"))
		if b != nil {
			v := b.Get([]byte(key))
			if v != nil {
				value = true
				return nil
			}

			return errors.New("key not found")

		}
		return errors.New("bucket not found")
	})

	return value, err
}

func (b *_BoltDB) GetOrDefault(key string, def string) string {

	v, e := b.Get(key)

	if e != nil {
		return def
	}

	return v
}

// get keys
func (b *_BoltDB) Gets(keys []string) (map[string]string, error) {
	if !b.isOpen {
		return nil, errors.New("db is not open")
	}

	data := make(map[string]string, 0)

	err := b.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(b.Bucket))
		if b != nil {
			for _, k := range keys {
				v := b.Get([]byte(k))
				if v != nil {
					data[k] = string(v)
				}
			}
		}
		return nil
	})

	// for _, k := range keys {
	// 	v, e := b.Get(k)
	// 	if e != nil {
	// 		data[k] = ""
	// 	} else {
	// 		data[k] = string(v)
	// 	}
	// }

	return data, err
}

// set json
func (b *_BoltDB) SetJson(key string, value interface{}) error {

	jv, e := json.Marshal(value)

	if e != nil {
		return e
	}

	return b.SetBytes([]byte(key), jv, 0)
}

// get json
func (b *_BoltDB) GetJson(key string, value interface{}) error {

	v, e := b.Get(key)
	if e != nil {
		return e
	}

	if len(v) == 0 {
		return errors.New("value is empty")
	}

	jsonErr := json.Unmarshal([]byte(v), value)

	if jsonErr != nil {
		return jsonErr
	}

	return nil
}

func (b *_BoltDB) Foreach(fun func(key string, value string) error) error {
	if !b.isOpen {
		return errors.New("db is not open")
	}
	if b.db == nil {
		return errors.New("db is nil")
	}
	err := b.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(b.Bucket))
		if b != nil {
			b.ForEach(func(k, v []byte) error {
				return fun(string(k), string(v))
			})
		}
		return nil
	})

	return err
}

func (b *_BoltDB) Sets(keys map[string]string) error {
	if !b.isOpen {
		return errors.New("db is not open")
	}
	if b.db == nil {
		return errors.New("db is nil")
	}

	err := b.db.Batch(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(b.Bucket))
		if b != nil {
			for k, v := range keys {
				err := b.Put([]byte(k), []byte(v))
				if err != nil {
					return err
				}
			}
		}
		return nil
	})

	return err
}
func (b *_BoltDB) Dels(keys []string) error {
	if !b.isOpen {
		return errors.New("db is not open")
	}
	if b.db == nil {
		return errors.New("db is nil")
	}

	err := b.db.Batch(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(b.Bucket))
		if b != nil {
			for _, k := range keys {
				err := b.Delete([]byte(k))
				if err != nil {
					return err
				}
			}
		}
		return nil
	})

	return err
}
