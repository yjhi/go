package jginex

import (
	"gitee.com/yjhi/go/jgin"
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-contrib/sessions/redis"
)

type JSession struct {
	Secret string `json:"secret"`
	Name   string `json:"name"`
}

func (ss *JSession) RegisterForCookies(s *jgin.GinServer) {

	store := cookie.NewStore([]byte(ss.Secret))

	s.Server.Use(sessions.Sessions(ss.Name, store))

}

func (ss *JSession) RegisterForRedis(s *jgin.GinServer, host string, pass string) {
	store, _ := redis.NewStore(10, "tcp", host, pass, []byte(ss.Secret))
	s.Server.Use(sessions.Sessions(ss.Name, store))
}

func NewSession(name string, secret string) *JSession {
	return &JSession{
		Secret: secret,
		Name:   name,
	}
}
