package jginex

import (
	"net/http"

	jwt "gitee.com/yjhi/go/jwt"
	"github.com/gin-gonic/gin"
)

func _JwtAuthMiddlewareConfig(name string, code string, message string) gin.HandlerFunc {
	return func(c *gin.Context) {
		auth := c.Request.Header.Get(TokenHeader)
		if auth == "" {
			c.JSON(
				http.StatusUnauthorized,
				gin.H{
					code:    1,
					message: "请重新登录",
				},
			)
			c.Abort()
			return
		}

		tk, er := jwt.ParseJWTToken(auth)
		if er != nil {
			c.JSON(
				http.StatusUnauthorized,
				gin.H{
					code:    1,
					message: "登录信息已失效",
				},
			)
			c.Abort()
			return
		}

		if tk != nil {
			c.Set("User", tk.Data)
		}
		c.Next()
	}
}
