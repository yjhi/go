package jtask

import (
	"errors"

	"github.com/robfig/cron/v3"
)

///define the cron wrapper
type CronTask struct {
	CronInstance *cron.Cron
	WithSeconds  bool
	_Started     bool
	_Stoped      bool
}

///start the cron
func (cm *CronTask) StartTask() error {
	if cm == nil {
		return errors.New("未进行初始化")
	}

	cm.CronInstance.Start()
	cm._Started = true
	return nil
}

///stop the task
func (cm *CronTask) StopTask() error {
	if cm == nil {
		return errors.New("未进行初始化")
	}
	cm.CronInstance.Stop()
	cm._Stoped = true
	return nil
}

///add a task
func (cm *CronTask) AddTask(t *CronTaskJob) error {

	if cm == nil {
		return errors.New("未进行初始化")
	}

	if cm._Started && !cm._Stoped {

		v, err := cm.CronInstance.AddJob(t.Time, t)

		if err != nil {
			return err
		}
		t.RunId = int(v)
		return nil
	}
	return errors.New("添加失败(未启动/已停止)")
}

///add a task
func (cm *CronTask) AddTaskWithData(t *CronDataTaskJob) error {

	if cm == nil {
		return errors.New("未进行初始化")
	}

	if cm._Started && !cm._Stoped {

		v, err := cm.CronInstance.AddJob(t.Time, t)

		if err != nil {
			return err
		}

		t.RunId = int(v)

		return nil
	}
	return errors.New("添加失败(未启动/已停止)")
}

///add a task
func (cm *CronTask) DelTask(id int) error {
	if cm == nil {
		return errors.New("未进行初始化")
	}

	if cm._Started && !cm._Stoped {
		cm.CronInstance.Remove(cron.EntryID(id))
		return nil
	}

	return errors.New("添加失败(未启动/已停止)")
}
