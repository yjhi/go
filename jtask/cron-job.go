package jtask

import "gitee.com/yjhi/go/jutils"

///define the job
type CronTaskJob struct {
	Time    string
	TaskId  string
	WorkFun func(s string)
	RunId   int
}

///define the task
func (ct *CronTaskJob) Run() {
	go ct.WorkFun(ct.TaskId)
}

func NewJob(time string, fun func(s string), data interface{}) *CronTaskJob {

	return &CronTaskJob{
		Time:    time,
		TaskId:  jutils.NewGuid(),
		WorkFun: fun,
	}
}
