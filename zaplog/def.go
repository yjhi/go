package zaplog

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

type ZapLoggerConfig struct {
	Filename   string
	MaxSize    int           /*20 Mb*/
	MaxBackups int           /*0*/
	MaxAge     int           /*15 Day*/
	Compress   bool          /*true*/
	Json       bool          /*false*/
	WithCaller bool          /*true*/
	WithStack  bool          /*true*/
	Level      zapcore.Level /*InfoLevel*/
	StackLevel zapcore.Level /*WarnLevel*/
}

type ZapLogger struct {
	RotateLogger  *lumberjack.Logger
	logger        *zap.Logger
	EncoderConfig zapcore.EncoderConfig

	config *ZapLoggerConfig
}
